package modules

import (
	"geoservicerpc/proxy/internal/infrastructure/responder"
	acontroller "geoservicerpc/proxy/internal/modules/auth/controller"
	gcontroller "geoservicerpc/proxy/internal/modules/geo/controller"
	"geoservicerpc/proxy/internal/modules/geo/service"
)

type Controllers struct {
	Auth acontroller.AuthControllerer
	Geo  gcontroller.GeoControllerer
}

func NewControllers(services *Services, serviceRPC *service.GeoServicRPC, respond responder.Responder) *Controllers {
	return &Controllers{
		Auth: acontroller.NewAuthController(services.Auth, respond),
		Geo:  gcontroller.NewGeoController(serviceRPC, respond),
	}

}
