package modules

import (
	"database/sql"
	"geoservicerpc/proxy/config"
	"geoservicerpc/proxy/internal/entities/authEntity"
	aservice "geoservicerpc/proxy/internal/modules/auth/service"
	gservice "geoservicerpc/proxy/internal/modules/geo/service"
	"github.com/go-chi/jwtauth"
	"github.com/redis/go-redis/v9"
)

type Services struct {
	Geo  gservice.GeoServicer
	Auth aservice.AuthServicer
}

func NewServices(db *sql.DB, cacheRedis *redis.Client, conf *config.Config,
	storage *authEntity.StorageAuth, token *jwtauth.JWTAuth) *Services {
	return &Services{
		Geo:  gservice.NewGeoService(db, cacheRedis, conf),
		Auth: aservice.NewAuthService(storage, token),
	}
}
