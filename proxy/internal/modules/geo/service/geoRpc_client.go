package service

import (
	"geoservicerpc/proxy/internal/entities/geoEntity"
	"log"
	"net/rpc"
)

type GeoServicRPC struct {
	client *rpc.Client
}

func NewGeoServicRPC(client *rpc.Client) *GeoServicRPC {
	return &GeoServicRPC{client: client}
}

func (g *GeoServicRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error) {
	var out geoEntity.Suggestions
	err := g.client.Call("GeoServiceRPC.PrepareGeocodeRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicRPC|PrepareGeocodeRequest:", err)
		return nil, err
	}

	return &out, nil
}
func (g *GeoServicRPC) PrepareSearchRequest(in geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {
	var out geoEntity.SearchResponse
	err := g.client.Call("GeoServiceRPC.PrepareSearchRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicRPC|PrepareSearchRequest:", err)
		return nil, err
	}
	return &out, nil
}
